package com.example.create_20120703;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

public class getProduct extends Activity {
	private int products_id;
	private JSONObject product_info;
	private TextView name, qty, price, date;
	private WebView description;
	private Button back;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_info);
		
		Intent intent = getIntent();
		try {
			products_id = intent.getIntExtra("products_id", 0);
			if( products_id == 0){
				finish();
			}
			
			this.setTitle( intent.getStringExtra("title") );
		} catch (Exception e) {
			e.printStackTrace();
			finish();
		}

		name = (TextView)findViewById(R.id.name);
		qty = (TextView)findViewById(R.id.qty);
		price = (TextView)findViewById(R.id.price);
		date = (TextView)findViewById(R.id.date);
		description = (WebView)findViewById(R.id.description);
		description.getSettings().setSupportZoom(true);
		description.getSettings().setBuiltInZoomControls(true);
		back = (Button)findViewById(R.id.back);
		back.setText("返回");
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		
		getProductInfo();
	}

	private void getProductInfo() {
		// 使用Get方法連線伺服器
		HttpGet request = new HttpGet( String.format("http://192.168.0.104/twe3/mobile.php?act=product&products_id=%d", products_id));

		try {
			// 與伺服器溝通
			HttpResponse response = new DefaultHttpClient().execute(request);

			// 伺服器回報成功
			if (response.getStatusLine().getStatusCode() == 200) {
				// 伺服器回傳值
				String result = EntityUtils.toString(response.getEntity());

				// 解析JSON物件
				JSONObject object = new JSONObject(result);

				// 設定商品名稱
				name.setText( object.getString("products_name") );
				qty.setText( String.format("商品數量: %d", object.getInt("products_quantity")) );
				price.setText( String.format("商品價格: NT$%d 元", object.getInt("products_price")) );
				date.setText( String.format("商品上架日期: %s", object.getString("products_date_added")) );
				description.loadDataWithBaseURL(null, object.getString("products_description"), "text/html", "utf-8", null);

			}
		} catch (Exception e) {
			// 發生錯誤
			e.printStackTrace();
		}
	}
}
