package com.example.create_20120703;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.StrictMode;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {
	private ListView list;
	private JSONObject[] categories;
	private JSONObject[] products;
	private boolean cat_status = false;
	private String[] categories_name;
	private String[] products_name;
	private JSONArray json;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// 元件參考
		list = (ListView) findViewById(R.id.list);

		// 設定網路
		setNetwork();

		// 設定標題
		getConfiguration();

		// 取得目錄
		getCategory();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	// 取得目錄
	private void getCategory() {
		// 使用Get方法連線伺服器
		HttpGet request = new HttpGet("http://192.168.0.104/twe3/mobile.php?act=category");
		
		try {
			if( cat_status ){
				setListViewCategories();
			} else {
				// 與伺服器溝通
				HttpResponse response = new DefaultHttpClient().execute(request);
				
				// 伺服器回報成功
				if (response.getStatusLine().getStatusCode() == 200) {
					// 伺服器回傳值
					String result = EntityUtils.toString(response.getEntity());
					
					// 解析JSON陣列
					json = new JSONArray(result);
					
					JSONObject object;
					categories = new JSONObject[json.length()];
					
					categories_name = new String[json.length()];
					for(int c=0; c<json.length(); c++){
						// 解析JSON物件
						object = new JSONObject( json.getString(c) );
						categories_name[c] = object.getString("categories_name");
						categories[c] = object;
					}
					
					setListViewCategories();
					cat_status = true;
				}
			}
		} catch (Exception e) {
			// 發生錯誤
			e.printStackTrace();
		}
	}

	private void setListViewCategories() {
		// 設定適配器(底座)
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, categories_name);
		// 設定選單底座
		list.setAdapter(adapter);
		// 設定選單點擊事件
		list.setOnItemClickListener(new OnItemClickListener() {

			/*
			 * 選單點擊方法
			 * @param arg2 點擊順序編號
			 */
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				try {
					// 取得商品
					getProducts( categories[arg2].getInt("categories_id") );
				} catch (JSONException e) {
					// JSON解析錯誤
					e.printStackTrace();
				}
			}
		});
	}

	// 取得商品
	private void getProducts(int categories_id) {
		// 使用Get方法連線伺服器
		HttpGet request = new HttpGet( String.format("http://192.168.0.104/twe3/mobile.php?act=products&category_id=%d", categories_id) );
		
		try {
			// 與伺服器溝通
			HttpResponse response = new DefaultHttpClient().execute(request);
			
			// 伺服器回報成功
			if (response.getStatusLine().getStatusCode() == 200) {
				// 伺服器回傳值
				String result = EntityUtils.toString(response.getEntity());
							
				// 解析JSON陣列
				json = new JSONArray(result);
				
				JSONObject object;
				products = new JSONObject[json.length()];
				
				products_name = new String[json.length()+1];
				for(int p=0; p<json.length(); p++){
					// 解析JSON物件
					object = new JSONObject( json.getString(p) );
					products_name[p] = object.getString("products_name");
					products[p] = object;
				}
				products_name[json.length()] = "回上層";
				
				setListViewProducts();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setListViewProducts() {
		// 設定適配器(底座)
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, products_name);
		// 設定選單底座
		list.setAdapter(adapter);
		// 設定選單點擊事件
		list.setOnItemClickListener(new OnItemClickListener() {

			/*
			 * 選單點擊方法
			 * @param arg2 點擊順序編號
			 */
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				try {
					// 進入商品
					Intent intent = new Intent(MainActivity.this, getProduct.class);
					if( arg2 == json.length() ){
						getCategory();
					} else {
						intent.putExtra("title", MainActivity.this.getTitle() );
						intent.putExtra("products_id", products[arg2].getInt("products_id"));
						startActivity(intent);
					}
				} catch (JSONException e) {
					// JSON解析錯誤
					e.printStackTrace();
				}
			}
		});
	}

	// 取得組態設定
	private void getConfiguration() {
		// 使用Get方法連線伺服器
		HttpGet request = new HttpGet(
				"http://192.168.0.104/twe3/mobile.php?act=configuration&configuration_key=STORE_NAME");
		
		try {
			// 與伺服器溝通
			HttpResponse response = new DefaultHttpClient().execute(request);
			
			// 伺服器回報成功
			if (response.getStatusLine().getStatusCode() == 200) {
				// 伺服器回傳值
				String result = EntityUtils.toString(response.getEntity());
				
				// 解析JSON陣列
				JSONArray json = new JSONArray(result);
				
				// 解析JSON物件
				JSONObject object = new JSONObject( json.getString(0) );
				
				// 設定標題
				this.setTitle( object.getString("configuration_value") );
			}
		} catch (Exception e) {
			// 發生錯誤
			e.printStackTrace();
		}
	}

	private void setNetwork() {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
				.detectDiskReads().detectDiskWrites().detectNetwork() // or
																		// .detectAll()
																		// for
																		// all
																		// detectable
																		// problems
				.penaltyLog().build());
		StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
				.detectLeakedSqlLiteObjects().penaltyLog().penaltyDeath()
				.build());
	}

}
